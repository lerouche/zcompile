"use strict";

const fs = require("fs-extra");
const Path = require("path");
const Worker = require("tiny-worker");

const call_onload = require("./utils/call_onload");
const default_settings = require("./default_settings");
const get_type = require("./utils/get_type");
const is_dir_sync = require("./utils/is_dir_sync");
const make_settings = require("./utils/make_settings");
const report_settings = require("./utils/report_settings");

const zc_minify = require("./minification/zc_minify");

const WORKER_DIR = __dirname + "/workers/";

const zcompile = settings => {
  settings = make_settings(default_settings, settings);
  const {
    source,
    destination,

    types,

    files,
    copy,

    logger,
    threadsCount,
  } = settings;

  report_settings(settings, logger);

  // Clean paths and add slash at the end
  // NOTE: Undefined behaviour if source path doesn't exist; destination path doesn't have to exist
  const SRC_DIR = Path.normalize(source + "/");
  const DST_DIR = Path.normalize(destination + "/");
  if (!is_dir_sync(SRC_DIR)) {
    throw new Error(`"${SRC_DIR}" is not a valid directory`);
  }

  logger(`Source:      ${SRC_DIR}`, 5);
  logger(`Destination: ${DST_DIR}`, 5);

  let file_configs = files.map(file_config => {
    if (typeof file_config == "string") {
      // First two elems are provided to ProcessingStack.processFile later,
      // last elem is to keep track of path relative to SRC_DIR and DST_DIR

      // Get type now as it's needed not just for processing
      return [SRC_DIR + file_config, get_type(file_config, types), file_config];

    } else if (file_config && typeof file_config == "object") {
      if (typeof file_config.source != "string") {
        throw new TypeError(`File .source is not a string`);
      }

      let type = file_config.type || get_type(file_config.source, types);
      let destination = file_config.destination || file_config.source;
      return [SRC_DIR + file_config.source, type, destination];

    } else {
      throw new TypeError(`Invalid file value: ${file_config}`);
    }
  });

  return new Promise((resolve, reject) => {
    new Promise((resolve, reject) => {
      let processed_files = new Array(file_configs.length);
      let next_in_queue = 0;
      let waiting = 0;
      let failed = false;

      let end = (err, data) => {
        threads.forEach(t => t.worker.terminate());
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      };

      let ready = thread => {
        if (next_in_queue < file_configs.length) {
          // Unprocessed files remain
          thread.current = next_in_queue;
          waiting++;
          next_in_queue++;
          thread.worker.postMessage([
            file_configs[thread.current][0],
            file_configs[thread.current][1]
          ]);

        } else {
          if (!waiting) {
            // All complete
            end(null, processed_files);
          }
        }
      };

      let onmessage = (e, thread) => {
        let [err, processed] = e.data;

        if (err) {
          if (!failed) {
            failed = true;
            let err_obj = new global[err.name](err.message);
            err_obj.stack = err.stack;
            end(err_obj);
          }

        } else {
          processed_files[thread.current] = processed;
          thread.current = undefined;
          waiting--;
          ready(thread);
        }
      };

      let onerror = err => {
        if (!failed) {
          failed = true;
          err.message = `INTERR_THREAD ${err.message}`;
          end(err);
        }
      };

      let threads = new Array(threadsCount).fill(true)
        .map(() => {
          return {
            current: undefined,
            worker: new Worker(WORKER_DIR + "process_base_file.worker.js"),
          };
        });

      for (let thread of threads) {
        // Set up worker by instructing it where to load ProcessingStack and what its arguments are
        thread.worker.postMessage([__dirname + "/directives/ProcessingStack.js", types]);
        // Closure is faster than .bind
        thread.worker.onmessage = e => onmessage(e, thread);
        thread.worker.onerror = onerror;
        ready(thread);
      }
    })
      .then(processed_files => {
        return Promise.all(processed_files.map((processed, file_no) => {
          let [, type, ref] = file_configs[file_no];
          return call_onload(settings.onloadfile, processed, ref, false)
            .then(code => call_onload(types[type].onload, code, ref, false))
            .then(code => zc_minify(type, code, settings, ref));
        }));
      })
      .then(minified_files =>
        Promise.all(minified_files.map((minified, file_no) =>
          fs.outputFile(DST_DIR + file_configs[file_no][2], minified))))
      .then(() => {
        resolve();
      })
      .catch(err => {
        err.message += "\n";
        logger(`[ERR] ${err}`, 1);
        reject(err);
      });
  });
};

zcompile.default_settings = default_settings;

module.exports = zcompile;
