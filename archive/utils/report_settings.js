"use strict";

const report_settings = (settings, logger) => {
  let logMessage = ["Minifying:"];

  if (settings.minifySelectors) {
    logMessage.push("Selectors");
  }
  if (settings.minifyCSS) {
    logMessage.push("CSS files");
  }
  if (settings.minifyJS) {
    logMessage.push("JavaScript files" + (settings.minifyJS.harmony ?
      " (harmony)" :
      ""));
  }
  if (settings.minifyHTML) {
    logMessage.push("HTML files" + (settings.minifyHTML.minifyInlineJS ?
      " + inline JavaScript" :
      "") + (settings.minifyHTML.minifyInlineCSS ?
      " + inline CSS" :
      ""));
  }
  if (settings.minifyXML) {
    logMessage.push("XML files");
  }
  if (settings.minifyJSON) {
    logMessage.push("JSON files");
  }
  if (settings.minifySVG) {
    logMessage.push("SVG files");
  }

  if (logMessage.length == 1) {
    logMessage = ["Not minifying anything"];
  }

  logMessage = logMessage.join("\n  - ");
  logger(logMessage, 5);
};

module.exports = report_settings;
