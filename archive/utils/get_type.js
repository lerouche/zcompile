"use strict";

const Path = require('path');

const get_type = (path, types) => {
  let ext = Path.extname(path).slice(1);

  for (let t of Object.keys(types)) {
    if (types[t].extensions.includes(ext)) {
      return t;
    }
  }

  throw new TypeError(`Unknown type for file "${path}"`);
};

module.exports = get_type;
