"use strict";

/*
 *  Evaluate Promises serially.
 *  Instead of providing a Promise for each element,
 *  provide a function that returns a Promise.
 *
 *  This is because creating a Promise immediately
 *  evaluates it.
 *
 *  Like Promise.all, functions that return values
 *  that are not Promises are resolved immediately.
 */
const promise_serial = queue => {
  if (!queue.length) {
    return Promise.resolve([]);
  }

  return new Promise((resolve, reject) => {
    let resolved = new Array(queue.length);
    let lastThen = Promise.resolve();
    queue.forEach((qe, i) => {
      lastThen = lastThen.then(last_val => {
        if (i > 0) {
          resolved[i - 1] = last_val;
        }
        return Promise.resolve(qe());
      });
    });
    lastThen
      .then(last_val => {
        resolved[queue.length - 1] = last_val;
        resolve(resolved);
      })
      .catch(reject);
  });
};

module.exports = promise_serial;
