"use strict";

const call_onload = (onload, code, ref, is_inline) => {
  if (typeof onload != "function") {
    return Promise.resolve(code);
  }
  return new Promise((resolve, reject) => {
    // If $onload is sync and throws error, Promise will auto reject
    Promise.resolve(onload(code, ref, is_inline))
      .then(res => {
        if (typeof res == "string") {
          resolve(res);
        } else {
          resolve(code);
        }
      })
      .catch(reject);
  });
};

module.exports = call_onload;
