"use strict";

function is_obj_lit (thing) {
  return !!thing && typeof thing == "object" && (thing.constructor === Object || Object.getPrototypeOf(thing) === null);
}

function clone (thing) {
  if (Array.isArray(thing)) {
    return thing.map(clone);
  }

  if (is_obj_lit(thing)) {
    let ret = Object.create(null);
    Object.keys(thing)
      .forEach(propName => {
        ret[propName] = clone(thing[propName]);
      });
    return ret;
  }

  return thing;
}

const make_settings = (defaults, provided) => {
  let result = Object.create(null);

  Object.keys(defaults)
    .forEach(propName => {
      let defaultValue = defaults[propName];
      let providedValue = provided[propName];

      if (providedValue === undefined) {
        result[propName] = defaultValue;
      } else if (is_obj_lit(defaultValue)) {
        if (providedValue === true) {
          result[propName] = defaultValue;
        } else if (!is_obj_lit(providedValue)) {
          result[propName] = clone(providedValue);
        } else {
          result[propName] = make_settings(defaultValue, providedValue);
        }
      } else {
        result[propName] = clone(providedValue);
      }
    });

  return result;
};

module.exports = make_settings;
