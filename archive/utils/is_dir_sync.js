"use strict";

const fs = require("fs");

const is_dir_sync = path => {
  try {
    return fs.lstatSync(path).isDirectory();
  } catch (e) {
    if (e.code === "ENOENT") {
      return false;
    }
    throw e;
  }
};

module.exports = is_dir_sync;
