"use strict";

const CleanCSS = require("clean-css");

const minifier = new CleanCSS({
  processImport: false,
  returnPromise: true,
});

const zc_minify_css = (code, settings) => {
  return new Promise((resolve, reject) => {
    if (!settings.types.css.minify) {
      resolve(code);
      return;
    }

    minifier.minify(code)
      .then(output => {
        if (output.warnings.length) {
          return Promise.reject(output.warnings);
        }
        resolve(output.styles);
      })
      .catch(errs => {
        let msg = errs[0];

        let pos = -1;
        let pos_match = /at ([0-9]+):([0-9]+)/.exec(msg);

        if (pos_match) {
          let line = Number.parseInt(pos_match[1], 10);
          let col = Number.parseInt(pos_match[2], 10);

          for (let i = 0; i < line; i++) {
            let done = false;
            while (!done) {
              pos++;

              switch (code[pos]) {
              case "\r":
                if (code[pos + 1] == "\n") {
                  pos++;
                }
                // Fallthrough

              case "\n":
                done = true;
                break;
              }
            }
          }

          pos += col;
        }

        let err = new SyntaxError(msg);
        err.pos = pos;
        reject(err);
      });
  });
};

module.exports = zc_minify_css;
