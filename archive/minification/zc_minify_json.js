"use strict";

const zc_minify_json = (code, settings) => {
  return new Promise((resolve, reject) => {
    if (!settings.types.json.minify) {
      resolve(code);
      return;
    }

    // Promise will auto reject if this fails
    resolve(JSON.stringify(JSON.parse(code)));
  });
};

module.exports = zc_minify_json;
