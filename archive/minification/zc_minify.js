"use strict";

const zc_minify_css = require("./zc_minify_css");
const zc_minify_html = require("./zc_minify_html");
const zc_minify_js = require("./zc_minify_js");
const zc_minify_json = require("./zc_minify_json");
const zc_minify_svg = require("./zc_minify_svg");
const zc_minify_xml = require("./zc_minify_xml");

const MINIFIERS = {
  css: zc_minify_css,
  js: zc_minify_js,
  html: zc_minify_html,
  json: zc_minify_json,
  svg: zc_minify_svg,
  xml: zc_minify_xml,
};

const zc_minify = (type, code, settings, ref) => {
  return new Promise((resolve, reject) => {
    code = code.trim();
    if (!code) {
      resolve(code);
      return;
    }

    let minifier = MINIFIERS[type];
    if (!minifier) {
      resolve(code);
      return;
    }

    minifier(code, settings, ref)
      .then(minified => {
        resolve(minified.trim());
      })
      .catch(err => {
        let position = err.pos || 0;
        let area = settings.minifyErrorSnippetSize;
        let startPos = Math.max(0, err.pos - area / 2);
        let leftArea = position - startPos;
        let endPos = position + (area - leftArea);
        err.message += `\n    at ${ref}\n` +
                       "\n==================== SNIPPET ====================\n" +
                       code.slice(startPos, endPos + 1) +
                       "\n================== END SNIPPET ==================";
        reject(err);
      });
  });
};

module.exports = zc_minify;
