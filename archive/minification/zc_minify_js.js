"use strict";

const UglifyJS = require("uglify-js");
const UglifyES = require("uglify-es");

const zc_minify_js = (code, settings) => {
  return new Promise((resolve, reject) => {
    if (!settings.types.js.minify) {
      resolve(code);
      return;
    }

    let compressSettings = {
      booleans: true,
      collapse_vars: true,
      comparisons: true,
      conditionals: true,
      dead_code: true,
      drop_console: !settings.types.js.minify.debug,
      drop_debugger: !settings.types.js.minify.debug,
      evaluate: true,
      hoist_funs: true,
      hoist_vars: false,
      if_return: true,
      join_vars: true,
      keep_fargs: false,
      keep_fnames: false,
      loops: true,
      negate_iife: true,
      passes: settings.types.js.minify.passes || 1,
      properties: true,
      reduce_vars: true,
      sequences: true,
      unsafe: true,
      unused: true,
      // Return compressor warnings in result.warnings, dependant on settings.types.js.minify.warnings
      warnings: true,
    };

    // Promise will auto reject if this fails
    // UglifyJS/ES does set .pos on error
    let result = (settings.types.js.minify.harmony ? UglifyES : UglifyJS).minify(code, {
      parse: {
        shebang: true,
        bare_returns: true,
      },
      warnings: false,
      mangle: true,
      compress: compressSettings,
    });

    if (result.error) {
      reject(result.error);
    } else {
      resolve(result.code);
    }
  });
};

module.exports = zc_minify_js;
