// The path to ProcessingStack, as well as its constructor argument $extensions,
// will be provided by the parent as its first message
let ProcessingStack;
let types;

function process_base_file (path, type) {
  return new Promise((resolve, reject) => {
    let proc_stack = new ProcessingStack(types);
    proc_stack.processFile(path, type)
      .then(processed => {
        resolve(processed);
      })
      .catch(err => {
        reject(err);
      });
  });
}

this.onmessage = e => {
  ProcessingStack = require(e.data[0]);
  types = e.data[1];

  this.onmessage = e => {
    process_base_file(...e.data)
      .then(processed => {
        postMessage([null, processed]);
      })
      .catch(err => {
        postMessage([{
          name: err.name,
          message: err.message,
          stack: err.stack,
        }]);
      });
  };
};
