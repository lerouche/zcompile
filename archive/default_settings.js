"use strict";

const os = require("os");

const default_settings = {
  /*
   *  Logger levels:
   *    2) Debug
   *    5) Info
   */
  logger: (message, level) => void 0,

  threadsCount: os.cpus().length,

  minifySelectors: {
    namespaces: [
      {
        // Paths to exported selectors files, or objects like:
        //   { classes: { "original" : "min", "orig2", "min2" }, IDs: { "orig-1": "min1", "orig2": "m" } }
        imports: [],
        // Check if selector is valid (i.e. should be minified),
        // and what parts to use as key (i.e. `matches.slice(1).join("") || matches[0]`)
        match: /^[a-z]+(?:-[a-z0-9]+)*$/, // or (sel, is_id, file_path, file_type) => sel
        prefix: "",
        suffix: "",
        onminify: null, // (key, is_id, minifier) => minifier(key, is_id)
        // A path to write minified selectors (not inc. imports)
        exportTo: null,
      },
    ],
  },

  // onload* callbacks are called after any directives have been parsed and evaluated,
  //   and are not called during evaluation (e.g. onloadjs is not called when a *.js file is imported)
  // All language event callbacks are also called for nested languages (e.g. JS in HTML)
  // If an HTML file has inner JS, the functions that will be called in order are: onloadfile html.onload js.onload
  onloadfile: null, // (code, path, type) => code
  onminifyfile: null, // (code, path, type) => code
  minifyErrorSnippetSize: 800,

  types: {
    css: {
      extensions: ["css"],
      onload: null, // (code, path, is_inline) => code
      minify: true,
    },

    js: {
      extensions: ["js"],
      onload: null, // (code, path, is_inline) => code
      minify: {
        harmony: false,
        passes: 1,
        debug: false,
      },
    },

    html: {
      extensions: ["html"],
      onload: null, // (code, path) => code
      eventAttributes: [/^on/],
      minify: {
        minifyJS: true,
        minifyCSS: true,
      },
    },

    xml: {
      extensions: ["xml"],
      onload: null, // (code, path) => code
      minify: true,
    },

    json: {
      extensions: ["json"],
      onload: null, // (code, path) => code
      minify: true,
    },

    svg: {
      extensions: ["svg"],
      onload: null, // (code, path) => code
      minify: true,
    },
  },

  source: "./src/",
  destination: "./dist/",

  files: [],
  copy: [],
};

module.exports = default_settings;
