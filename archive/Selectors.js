"use strict";

const STARTERS = "abcdefghijklmnopqrstuvwxyz_";
const CHARS = "abcdefghijklmnopqrstuvwxyz0123456789_-";

module.exports = function (settings) {
  const SELECTOR_PREPEND = settings.prepend || "";

  let selectors = {
    classes: new Map(),
    ids: new Map()
  };

  let min_sel_state = {
    classes: [-1],
    ids: [-1],
  };

  let get_next_min_sel = key => {
    let state = min_sel_state[key];
    let done = false;

    for (let i = state.length - 1; i >= 0; i--) {
      let limit = (i == 0 ? STARTERS : CHARS).length;
      let new_val = ++state[i];

      if (new_val < limit) {
        done = true;
        break;

      } else if (state[i] == limit) {
        state[i] = 0;

      } else {
        // This should never happen
        throw new Error(`INTERR Out-of-bound state value ${state[i]} at ${i}`);

      }
    }

    if (!done) {
      // If this is reached, max value for length reached and new char needed
      state.unshift(0);
    }

    return state.map((idx, pos) => (pos == 0 ? STARTERS : CHARS)[idx])
      .join("");
  };

  let external_selectors = {
    classes: new Map(),
    ids: new Map(),
    min_classes: new Set(),
    min_ids: new Set(),
  };

  if (settings.externalSelectors) {
    external_selectors.classes = new Map(settings.externalSelectors.c);
    external_selectors.ids = new Map(settings.externalSelectors.i);
    external_selectors.min_classes = new Set(external_selectors.classes.values());
    external_selectors.min_ids = new Set(external_selectors.ids.values());
  }

  this.minifySelector = function (sel, is_a_class) {

    let key = is_a_class ?
      "classes" :
      "ids";

    if (external_selectors[key].has(sel)) {
      return external_selectors[key].get(sel);
    }

    if (!selectors[key].has(sel)) {
      let min_sel;

      do {
        min_sel = SELECTOR_PREPEND + get_next_min_sel(key);
      } while (external_selectors["min_" + key].has(min_sel));

      selectors[key].set(sel, min_sel);
    }

    return selectors[key].get(sel);

  };

  this.export = function () {
    return {
      c: [...selectors.classes],
      i: [...selectors.ids]
    };
  };
};
