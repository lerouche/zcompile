"use strict";

const DirectiveExpr = require("../DirectiveExpr");

// Called on match `//`
function consume_sl_comment (proc_stack) {
  let comment = "//";
  proc_stack.tSkip(2);
  let done = false;

  while (!proc_stack.tAtEnd() && !done) {
    let peek = proc_stack.tPeek();
    if (peek == "\r" || peek == "\n") {
      done = true;
    } else {
      comment += proc_stack.tAccept();
    }
  }

  return comment;
}

// Called on match `/*`
function consume_ml_comment (proc_stack) {
  let comment = "/*";
  proc_stack.tSkip(2);

  while (!proc_stack.tMatches("*/")) {
    comment += proc_stack.tAccept();
  }
  proc_stack.tSkip(2);
  comment += "*/";

  return comment;
}

// Called on match `"` or `'`
function consume_string (proc_stack) {
  let delim = proc_stack.tAccept();
  let str = delim;

  let escaping = 0;
  let done = false;

  while (!done) {
    let c = proc_stack.tAccept();
    if (c == "\\") {
      escaping ^= 1;

    } else {
      if (c == delim && !escaping) {
        done = true;

      } else if (c == "\r" || c == "\n") {
        if (!escaping) {
          throw new SyntaxError(`Unterminated JavaScript string`);
        }
        if (c == "\r" && proc_stack.tPeek() == "\n") {
          proc_stack.tSkip();
        }
      }

      escaping = 0;
    }

    str += c;
  }

  return str;
}

// Called on match `\``
function consume_template (proc_stack) {
  let template = "`";
  proc_stack.tSkip(1);

  let escaping = 0;
  let done = false;

  while (!done) {
    let c = proc_stack.tAccept();
    if (c == "\\") {
      escaping ^= 1;

    } else {
      if (c == "`" && !escaping) {
        done = true;
      }

      escaping = 0;
    }

    template += c;
  }

  return template;
}

const CONSUMERS_DIRECTIVE_ARG = {
  "\"": consume_string,
  "'": consume_string,
  "__zc_": consume_directive,
};

const CONSUMERS_DIRECTIVE_ARG_TRIGGERS = Object.keys(CONSUMERS_DIRECTIVE_ARG);

const WHITESPACE = new Set([" ", "\t", "\v", "\r", "\n"]);

// Called on match `__zc_`
function consume_directive (proc_stack) {
  let pos = proc_stack._tGetNPos();

  proc_stack.tSkip(5);
  let directive_name = proc_stack.tAcceptUntil("(");
  proc_stack.tSkip();

  let args = [];
  do {
    proc_stack.tSkipWhile(WHITESPACE);
    let peek = proc_stack.tPeek();
    if (peek == ")") {
      break;
    }

    let arg_val_parts = [];

    do {
      proc_stack.tSkipWhile(WHITESPACE);
      let matched = false;
      for (let trig of CONSUMERS_DIRECTIVE_ARG_TRIGGERS) {
        if (proc_stack.tMatches(trig)) {
          let consumed = CONSUMERS_DIRECTIVE_ARG[trig](proc_stack);
          if (consumed instanceof DirectiveExpr) {
            arg_val_parts.push(consumed);
          } else {
            arg_val_parts.push(Function(`return ${consumed}`)());
          }
          matched = true;
          break;
        }
      }
      if (!matched) {
        throw new SyntaxError(`Invalid JS directive argument`);
      }
      proc_stack.tSkipWhile(WHITESPACE);
    } while (proc_stack.tSkipIf("+"));

    args.push(arg_val_parts);

  } while (proc_stack.tSkipIf(","));

  if (!proc_stack.tSkipIf(")")) {
    throw new SyntaxError(`JS directive is missing closing parenthesis`);
  }
  proc_stack.tSkipIf(";");

  return new DirectiveExpr(proc_stack, pos, directive_name, args);
}

const CONSUMERS = {
  "//": consume_sl_comment,
  "/*": consume_ml_comment,
  "\"": consume_string,
  "'": consume_string,
  "`": consume_template,
  "__zc_": consume_directive,
};

const CONSUMER_TRIGGERS = Object.keys(CONSUMERS);

const process_js_directives = (proc_stack, break_on_match) => {
  let processed = [];
  let cur_unconsumed = "";

  while ((!break_on_match && !proc_stack.tAtEnd()) ||
         (break_on_match && !proc_stack.tMatches(break_on_match))) {
    let matched = false;
    for (let trig of CONSUMER_TRIGGERS) {
      if (proc_stack.tMatches(trig)) {
        processed.push(cur_unconsumed);
        cur_unconsumed = "";
        processed.push(CONSUMERS[trig](proc_stack));
        matched = true;
        break;
      }
    }
    if (!matched) {
      cur_unconsumed += proc_stack.tAccept();
    }
  }

  processed.push(cur_unconsumed);

  return processed;
};

module.exports = process_js_directives;
