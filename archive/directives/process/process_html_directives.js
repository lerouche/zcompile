"use strict";

const DirectiveExpr = require("../DirectiveExpr");
const HTMLEntities = require("html-entities").AllHtmlEntities;

const htmlEntities = new HTMLEntities();

// Called after `<!`
function consume_doctype_or_comment (proc_stack) {
  let html = "<!";
  proc_stack.tSkip(2);
  html += proc_stack.tAcceptUntil(">") + proc_stack.tAccept();
  return html;
}

// Used to process:
//   - directive entity argument values
//   - directive tag argument values
//   - attribute values
function consume_char_data_with_entity_directives (proc_stack, decode_entities, break_on_a, break_on_b) {
  let parts = [];
  let cur_literal = "";

  while (!proc_stack.tMatches(break_on_a) &&
         (!break_on_b || !proc_stack.tMatches(break_on_b))) {
    let matched = false;

    if (proc_stack.tMatches("&zc-")) {
      if (decode_entities) {
        cur_literal = htmlEntities.decode(cur_literal);
      }
      parts.push(cur_literal);
      cur_literal = "";
      parts.push(consume_directive_entity(proc_stack));
      matched = true;
      break;
    } else {
      cur_literal += proc_stack.tAccept();
    }
  }

  if (decode_entities) {
    cur_literal = htmlEntities.decode(cur_literal);
  }
  parts.push(cur_literal);
  return parts;
}

// Called after consuming `<zc-${directive_name}`
function consume_directive_tag (proc_stack, tag_name) {
  let pos = proc_stack._tGetNPos() - tag_name.length;
  let directive_name = tag_name.slice(3);

  let raw_args = {};

  proc_stack.tSkipWhile(WHITESPACE);
  while (!proc_stack.tSkipIf(">")) {
    let arg_name = proc_stack.tAcceptUntil("=");
    proc_stack.tSkip();

    if (!proc_stack.tSkipIf("\"")) {
      throw new SyntaxError(`HTML directive tag argument attr value is unquoted`);
    }

    raw_args[arg_name] = consume_char_data_with_entity_directives(proc_stack, true, "\"");
    proc_stack.tSkip();

    proc_stack.tSkipWhile(WHITESPACE);
  }

  let closing_tag = `</zc-${directive_name}>`;
  // HTML entities in content will not be decoded (i.e. treat all HTML as text)
  if (raw_args.value != null) {
    throw new SyntaxError("Value argument provided as attribute");
  }
  raw_args.value = consume_html_content(proc_stack, closing_tag);
  proc_stack.tSkip(closing_tag.length);

  return new DirectiveExpr(proc_stack, pos, directive_name, raw_args);
}

const WHITESPACE = new Set([" ", "\t", "\r", "\n"]);
const TAG_NAME_DELIMITER = new Set([...WHITESPACE, ">"]);
const ATTR_VAL_START_OR_TAG_END_DELIMITER = new Set(["\"", ">"]);

// Called on match `<`
function consume_opening_or_closing_tag (proc_stack) {
  proc_stack.tSkip();
  let is_closing = proc_stack.tSkipIf("/");
  let tag_name = proc_stack.tAcceptUntilSet(TAG_NAME_DELIMITER).toLocaleLowerCase();
  if (!/^[a-z0-9]+(-[a-z0-9]+)*(:[a-z0-9]+(-[a-z0-9]+)*)?$/.test(tag_name)) {
    throw new SyntaxError(`Invalid HTML tag name "${tag_name}"`);
  }

  if (!is_closing && /^zc-/.test(tag_name)) {
    return consume_directive_tag(proc_stack, tag_name);
  }

  let parts = []; // Parts are needed for attribute value directives

  let html = "<" + (is_closing ? "/" : "") + tag_name;
  let done = false;
  while (!done) {
    html += proc_stack.tAcceptUntilSet(ATTR_VAL_START_OR_TAG_END_DELIMITER);
    let c = proc_stack.tAccept();
    html += c;

    switch (c) {
    case "\"":
      parts.push(html);
      Array.prototype.push.apply(parts, consume_char_data_with_entity_directives(proc_stack, false, "\""));
      html = proc_stack.tAccept();
      break;

    case ">":
      done = true;
      break;

    default:
      throw new Error(`INTERR Invalid char after ATTR_VAL_START_OR_TAG_END_DELIMITER`);
    }
  }

  parts.push(html);

  return parts;
}

// Called on match `&zc-`
function consume_directive_entity (proc_stack) {
  let pos = proc_stack._tGetNPos();

  proc_stack.tSkip(4);
  let directive_name = proc_stack.tAcceptUntil("(");
  proc_stack.tSkip();

  let raw_args = {};
  do {
    proc_stack.tSkipWhile(WHITESPACE);
    if (proc_stack.tPeek() == ")") {
      break;
    }

    let arg_name = proc_stack.tAcceptUntil("=");
    proc_stack.tSkip();

    raw_args[arg_name] = consume_char_data_with_entity_directives(proc_stack, true, ",", ")");
  } while (proc_stack.tSkipIf(","));

  if (!proc_stack.tSkipIf(")")) {
    throw new SyntaxError(`HTML directive entity is missing closing parenthesis`);
  }
  proc_stack.tSkipIf(";");

  return new DirectiveExpr(proc_stack, pos, directive_name, raw_args);
}

const CONSUMERS = {
  "<!": consume_doctype_or_comment,
  "<": consume_opening_or_closing_tag,
  "&zc-": consume_directive_entity,
};

const CONSUMER_TRIGGERS = Object.keys(CONSUMERS);

function consume_html_content (proc_stack, break_on_match) {
  let processed = [];
  let cur_unconsumed = "";

  while ((!break_on_match && !proc_stack.tAtEnd()) ||
         (break_on_match && !proc_stack.tMatches(break_on_match))) {
    let matched = false;
    for (let trig of CONSUMER_TRIGGERS) {
      if (proc_stack.tMatches(trig)) {
        processed.push(cur_unconsumed);
        cur_unconsumed = "";
        let consumed = CONSUMERS[trig](proc_stack);
        if (Array.isArray(consumed)) {
          Array.prototype.push.apply(processed, consumed);
        } else {
          processed.push(consumed);
        }
        matched = true;
        break;
      }
    }
    if (!matched) {
      cur_unconsumed += proc_stack.tAccept();
    }
  }

  processed.push(cur_unconsumed);

  return processed;
}

const process_html_directives = consume_html_content;

module.exports = process_html_directives;
