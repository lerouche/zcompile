"use strict";

const File = require("./File");

const process_js_directives = require("./process/process_js_directives");
const process_html_directives = require("./process/process_html_directives");
const promise_serial = require("../utils/promise_serial");

class ProcessingStack {
  constructor (types) {
    this.types = types;
    this.stack = [];
    this.imported = new Set();
  }

  _hasFile (path) {
    return this.stack.some(s => s.getPath() === path);
  }

  _tParseForDirectives () {
    switch (this.top().getType()) {
    case "js":
      return process_js_directives(this);
    case "html":
      return process_html_directives(this);
    default:
      return [this.top().getOriginalData()];
    }
  }

  _tEvaluateDirectives(parsed_parts) {
    return new Promise((resolve, reject) => {
      // WARNING: Directives must be processed async in order, not parallel
      //            - Variables must be accessed and assigned to in order
      //            - Imports must be processed in order to prevent stack sync problems
      //            - An import may alter variables, so both need to be in order
      promise_serial(parsed_parts.map(part => {
        // $part is either a string literal or DirectiveExpr
        if (typeof part == "string") {
          return () => part;
        }
        return () => part.evaluate(this);
      }))
        .then(parts => {
          resolve(parts.join(""));
        })
        .catch(reject);
    });
  }

  processFile (path, type) {
    return new Promise((resolve, reject) => {
      File.createFrom(path, type, this.types)
        .then(file => {
          this.stack.push(file);
          this._tSetNPos(0);

          let parsed_parts;
          try {
            // Don't parse and evaluate at the same time,
            // as then parse errors won't add stack to error message
            parsed_parts = this._tParseForDirectives()
          } catch (err) {
            err.message += this.tGenErr();
            this.stack.pop();
            reject(err);
            return;
          }

          this._tEvaluateDirectives(parsed_parts)
            .then(processed => {
              this.stack.pop();
              resolve(processed);
            })
            .catch(err => {
              this.stack.pop();
              reject(err);
            });
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  bottom () {
    return this.stack[0];
  }

  bGetVar (var_name) {
    return this.bottom().getState("proc:var:" + var_name);
  }

  bSetVar (var_name, var_value) {
    return this.bottom().setState("proc:var:" + var_name, var_value);
  }

  top () {
    return this.stack[this.stack.length - 1];
  }

  tGenErr () {
    let line = 1;
    let col = 0;
    let char_no = 0;
    let next_char_no = this._tGetNPos();

    while (char_no < next_char_no) {
      let c = this._tChar(char_no);
      switch (c) {
      case "\r":
        if (this._tChar(char_no + 1) == "\n") {
          char_no++;
        }
        // Fallthrough

      case "\n":
        line++;
        col = 0;
        break;

      default:
        col++;
      }

      char_no++;
    }

    return `\n    at ${this.top().getPath()}:${line}:${col}`;
  }

  _tData () {
    return this.top().getOriginalData();
  }

  _tChar (char_no) {
    if (!Number.isSafeInteger(char_no) || char_no < 0) {
      throw new Error(`INTERR Invalid position value: ${pos} (type ${typeof char_no})`);
    }
    if (!this._tBoundsCheck(char_no + 1)) {
      throw new Error(`Unexpected end of code`);
    }
    return this._tData()[char_no];
  }

  // Position is always the index of the next char
  _tGetNPos () {
    return this.top().getState("proc:nextCharNo");
  }

  _tSetNPos (nextCharNo) {
    if (!this._tBoundsCheck(nextCharNo)) {
      throw new Error(`Unexpected end of code`);
    }
    this.top().setState("proc:nextCharNo", nextCharNo);
  }

  _tBoundsCheck (pos) {
    if (!Number.isSafeInteger(pos) || pos < 0) {
      throw new Error(`INTERR Invalid position value: ${pos} (type ${typeof pos})`);
    }
    // Next char pos can be length (i.e. current pos is last char)
    return pos <= this._tData().length;
  }

  tAtEnd () {
    return !this._tBoundsCheck(this._tGetNPos() + 1);
  }

  tAccept () {
    let nextCharNo = this._tGetNPos();
    this._tSetNPos(nextCharNo + 1);
    return this._tChar(nextCharNo);
  }

  tAcceptWhile (char_set) {
    let accepted = "";
    while (char_set.has(this.tPeek())) {
      accepted += this.tAccept();
    }
    return accepted;
  }

  tAcceptUntil (char) {
    let accepted = "";
    while (this.tPeek() != char) {
      accepted += this.tAccept();
    }
    return accepted;
  }

  tAcceptUntilSet (char_set) {
    let accepted = "";
    while (!char_set.has(this.tPeek())) {
      accepted += this.tAccept();
    }
    return accepted;
  }

  tSkip (amount) {
    if (amount == null) {
      amount = 1;
    } else if (!Number.isSafeInteger(amount) || amount < 1) {
      throw new Error(`INTERR Invalid tSkip amount ${amount}`);
    }
    let nextCharNo = this._tGetNPos();
    this._tSetNPos(nextCharNo + amount);
  }

  tSkipWhile (char_set) {
    while (!this.tAtEnd() && char_set.has(this.tPeek())) {
      this.tSkip();
    }
  }

  tSkipIf (char) {
    if (!this.tAtEnd() && this.tPeek() == char) {
      this.tSkip();
      return true;
    }
    return false;
  }

  tPeek (offset) {
    if (offset == null) {
      offset = 1;
    } else if (!Number.isSafeInteger(offset) || offset < 1) {
      throw new Error(`INTERR Invalid tPeek offset ${offset}`);
    }
    return this._tChar(this._tGetNPos() + offset - 1);
  }

  tMatches (char_sequence) {
    if (this._tGetNPos() + char_sequence.length >= this._tData().length) {
      return false;
    }

    for (let i = 0; i < char_sequence.length; i++) {
      if (this._tChar(this._tGetNPos() + i) !== char_sequence[i]) {
        return false;
      }
    }

    return true;
  }
}

module.exports = ProcessingStack;
