"use strict";

const fs = require("fs");
const Path = require("path");

const get_type = require("../utils/get_type");

class File {
  static createFrom (path, type, types) {
    return new Promise((resolve, reject) => {
      path = Path.normalize(path);

      if (!type) {
        // This will throw if unknown
        type = get_type(path, types);
      } else {
        if (!types[type]) {
          throw new TypeError(`Invalid file type "${type}"`);
        }
      }

      let dirname = Path.dirname(path);
      let state = new Map();

      fs.readFile(path, "utf8", (err, original_data) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(new File({
          path, type, dirname, state, original_data,
        }));
      });
    });
  }

  constructor ({path, type, dirname, state, original_data}) {
    this.path = path;
    this.type = type;
    this.dirname = dirname;
    this.state = state;
    this.original_data = original_data;
  }

  getPath () {
    return this.path;
  }

  getType () {
    return this.type;
  }

  getDirectoryPath () {
    return this.dirname;
  }

  getOriginalData () {
    return this.original_data;
  }

  getState (key) {
    return this.state.get(key);
  }

  setState (key, value) {
    this.state.set(key, value);
  }
}

module.exports = File;
