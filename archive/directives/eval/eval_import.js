"use strict";

const Path = require("path");

const eval_import = (proc_stack, import_name, import_once_only) => {
  let import_abs_path;
  if (Path.isAbsolute(import_name)) {
    import_abs_path = import_name;
  } else {
    import_abs_path = proc_stack.top().getDirectoryPath() + "/" + import_name;
  }
  import_abs_path = Path.normalize(import_abs_path);

  if (import_once_only && proc_stack.imported.has(import_abs_path)) {
    return Promise.resolve("");
  }
  proc_stack.imported.add(import_abs_path);

  if (proc_stack._hasFile(import_abs_path)) {
    // Even if not detected, the reimported file will cause a cyclic import error
    return Promise.reject(new Error(`${import_abs_path} is a cyclic import`));
  }

  return proc_stack.processFile(import_abs_path);
};

module.exports = eval_import;
