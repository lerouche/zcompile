"use strict";

const eval_set_var = (proc_stack, name, value) => {
  proc_stack.bSetVar(name, value);
  return Promise.resolve("");
};

module.exports = eval_set_var;
