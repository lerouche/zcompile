"use strict";

const fs = require("fs");
const Path = require("path");

const eval_read_file = (proc_stack, file_path) => {
  let file_abs_path;
  if (Path.isAbsolute(file_path)) {
    file_abs_path = file_path;
  } else {
    file_abs_path = proc_stack.top().getDirectoryPath() + "/" + file_path;
  }
  file_abs_path = Path.normalize(file_abs_path);

  return new Promise((resolve, reject) => {
    fs.readFile(file_abs_path, "utf8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    })
  });
};

module.exports = eval_read_file;
