"use strict";

const eval_get_var = (proc_stack, name, def) => {
  return new Promise((resolve, reject) => {
    let val = proc_stack.bGetVar(name);

    if (val == null) {
      if (def == null) {
        reject(new SyntaxError(`No such variable exists: "${name}"`));
        return;
      }

      resolve(def);
      return;
    }

    resolve(val);
  });
};

module.exports = eval_get_var;
