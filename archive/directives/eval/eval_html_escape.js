"use strict";

const HTMLEntities = require("html-entities").AllHtmlEntities;

const entities = new HTMLEntities();

const eval_html_escape = (proc_stack, value) => {
  return Promise.resolve(entities.encode(value));
};

module.exports = eval_html_escape;
