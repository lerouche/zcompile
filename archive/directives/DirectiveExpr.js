"use strict";

const eval_import = require("./eval/eval_import");
const eval_read_file = require("./eval/eval_read_file");
const eval_get_var = require("./eval/eval_get_var");
const eval_set_var = require("./eval/eval_set_var");
const eval_html_escape = require("./eval/eval_html_escape");

const promise_serial = require("../utils/promise_serial");

const DIRECTIVES = {
  // Optional args must be at end
  "import": {fn: eval_import, sig: ["file", "once?"]},
  "readfile": {fn: eval_read_file, sig: ["file"]},
  "use": {fn: eval_get_var, sig: ["name", "def?"]},
  "set": {fn: eval_set_var, sig: ["name", "value"]},
  "htmlescape": {fn: eval_html_escape, sig: ["value"]},
};

for (let dir_name of Object.keys(DIRECTIVES)) {
  let pos_of_first_opt_arg = undefined;

  DIRECTIVES[dir_name].sig = DIRECTIVES[dir_name].sig.map((arg_name, arg_pos) => {
    if (arg_name[arg_name.length - 1] == "?") {
      if (pos_of_first_opt_arg == undefined) {
        pos_of_first_opt_arg = arg_pos;
      }
      return {optional: true, name: arg_name.slice(0, -1)};

    } else {
      if (pos_of_first_opt_arg != undefined) {
        throw new Error(`INTERR Required arg after optional arg for directive "${dir_name}"`);
      }
      return {optional: false, name: arg_name};
    }
  });

  DIRECTIVES[dir_name].reqArgsLen = pos_of_first_opt_arg == undefined ?
    DIRECTIVES[dir_name].sig.length :
    pos_of_first_opt_arg;
}

const DIRECTIVE_NAMES = new Set(Object.keys(DIRECTIVES));

function addStackToError (err, proc_stack, position) {
  proc_stack._tSetNPos(position + 1);
  err.message += proc_stack.tGenErr();
  return err;
}

class DirectiveExpr {
  // $proc_stack is for generating errors on construction,
  // $position is for generating errors on construction and evaluation
  constructor (proc_stack, position, directive_name, raw_args) {
    if (!DIRECTIVE_NAMES.has(directive_name)) {
      throw addStackToError(
        new ReferenceError(`Invalid directive "${directive_name}"`),
        proc_stack,
        position
      );
    }

    let dir = DIRECTIVES[directive_name];

    let args;

    if (Array.isArray(raw_args)) {
      if (raw_args.length < dir.reqArgsLen ||
          raw_args.length > dir.sig.length
      ) {
        throw addStackToError(
          new SyntaxError(`Too few or many arguments provided to directive`),
          proc_stack,
          position
        );
      }
      args = raw_args;

    } else if (raw_args instanceof Object) {
      args = [];
      for (let param of dir.sig) {
        let v = raw_args[param.name];
        if (v == null) {
          // Don't add if not provided, as later `evaluate` call maps every value
          if (!param.optional) {
            throw addStackToError(
              new SyntaxError(`Directive argument "${param.name}" missing`),
              proc_stack,
              position
            );
          }
        } else {
          args.push(v);
        }
      }

    } else {
      throw addStackToError(
        new TypeError(`INTERR Invalid raw_args value type: ${typeof raw_args}`),
        proc_stack,
        position
      );
    }

    this._position = position;
    this._dirfn = dir.fn;
    this._raw_args = args;
  }

  evaluate (proc_stack) {
    return new Promise((resolve, reject) => {
      promise_serial(
        this._raw_args.map(raw_arg_parts => {
          return () => new Promise((resolve, reject) => {
            promise_serial(
              raw_arg_parts.map(part => {
                // $part is either a string literal or DirectiveExpr
                if (typeof part == "string") {
                  return () => part;
                }
                // WARNING: Directives must be processed async in order, not parallel
                return () => part.evaluate(proc_stack);
              })
            )
              .then(arg_value_parts => {
                resolve(arg_value_parts.join(""));
              })
              // If something failed, it would be while a part.evaluate,
              // which will set the position, so don't do so here
              .catch(reject);
          });
        })
      )
        .then(args => {
          return this._dirfn(proc_stack, ...args);
        })
        .then(resolve)
        .catch(err => {
          // Since directives are evaluated asynchronously,
          // only set the position on error, as otherwise it
          // will just be overwritten by other async evals
          addStackToError(err, proc_stack, this._position);
          reject(err);
        });
    });
  }
}

module.exports = DirectiveExpr;
