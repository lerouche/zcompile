import {FileTypeSettings} from "./FileTypeSettings";
import {FileType} from "./FileTypeSettings";

export interface HTMLFileTypeSettings extends FileTypeSettings<FileType.HTML> {
  minify: {
    minifyJS: boolean;
    minifyCSS: boolean;
  };
}
