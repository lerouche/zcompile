import {FileTypeSettings} from "./FileTypeSettings";
import {FileType} from "./FileTypeSettings";

export interface JSONFileTypeSettings extends FileTypeSettings<FileType.JSON> {
  minify: boolean;
}
