import {FileTypeSettings} from "./FileTypeSettings";
import {FileType} from "./FileTypeSettings";

export interface XMLFileTypeSettings extends FileTypeSettings<FileType.XML> {
  minify: boolean;
}
