import {FileTypeSettings} from "./FileTypeSettings";
import {FileType} from "./FileTypeSettings";

export interface CSSFileTypeSettings extends FileTypeSettings<FileType.CSS> {
  minify: boolean;
}
