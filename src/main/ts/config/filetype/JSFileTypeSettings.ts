import {FileTypeSettings} from "./FileTypeSettings";
import {FileType} from "./FileTypeSettings";

export interface JSFileTypeSettings extends FileTypeSettings<FileType.JS> {
  minify: {
    harmony: boolean;
    debug: boolean;
  };
}
