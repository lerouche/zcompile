export enum FileType {
  JS = "js",
  CSS = "css",
  HTML = "html",
  XML = "xml",
  JSON = "json",
  SVG = "svg",
}

export interface FileTypeSettings<_ extends FileType> {
  extensions: ReadonlyArray<string>;
}
