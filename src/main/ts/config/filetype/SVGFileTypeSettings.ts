import {FileTypeSettings} from "./FileTypeSettings";
import {FileType} from "./FileTypeSettings";

export interface SVGFileTypeSettings extends FileTypeSettings<FileType.SVG> {
  minify: boolean;
}
