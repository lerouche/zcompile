export enum SelectorType {
  ID = "id",
  CLASS = "class",
}

export interface SelectorsPack {
  // { class: { "original" : "min", "orig2", "min2" }, ID: { "orig-1": "min1", "orig2": "m" } }
  [SelectorType.ID]: {
    [original: string]: string;
  };

  [SelectorType.CLASS]: {
    [original: string]: string;
  };
}

export interface SelectorsNamespaceSettings {
  // Paths to exported selectors files, or objects
  imports?: ReadonlyArray<string | SelectorsPack>;
  // Check if selector is valid (i.e. should be minified),
  // and what parts to use as key (i.e. `matches.slice(1).join("") || matches[0]`)
  match?: RegExp | ((sel: string, type: SelectorType, file_path: string, file_type: string) => string);
  prefix?: string;
  suffix?: string;
  // A path to write minified selectors (not including imports)
  exportTo?: null | string;
}
