import {FileSettings} from "./FileSettings";
import {FileType} from "./filetype/FileTypeSettings";
import {JSFileTypeSettings} from "./filetype/JSFileTypeSettings";
import {CSSFileTypeSettings} from "./filetype/CSSFileTypeSettings";
import {HTMLFileTypeSettings} from "./filetype/HTMLFileTypeSettings";
import {XMLFileTypeSettings} from "./filetype/XMLFileTypeSettings";
import {JSONFileTypeSettings} from "./filetype/JSONFileTypeSettings";
import {SVGFileTypeSettings} from "./filetype/SVGFileTypeSettings";
import {SelectorsNamespaceSettings} from "./selectors/SelectorsNamespaceSettings";

export interface BuilderSettings {
  logger?: null | ((message: string, level: number) => void);
  errorSnippetSize?: number;

  threadsCount?: number;

  minifySelectors?: false | Array<SelectorsNamespaceSettings>;

  types?: {
    [FileType.JS]?: JSFileTypeSettings;
    [FileType.CSS]?: CSSFileTypeSettings;
    [FileType.HTML]?: HTMLFileTypeSettings;
    [FileType.XML]?: XMLFileTypeSettings;
    [FileType.JSON]?: JSONFileTypeSettings;
    [FileType.SVG]?: SVGFileTypeSettings;
  };

  source: string;
  destination: string;

  files: Array<string | FileSettings>;
}
