export interface FileSettings {
  type: string;
  source: string;
  destination?: string;
  data?: string;
  copy?: boolean;
}
