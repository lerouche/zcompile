# zcompile

Opinionated, parallelised preprocessor and builder for HTML, CSS, JS, XML, JSON, and SVG

## Use case

zcompile is not your average build tool. It has a very specific recommended use case, and suggests using other build tools for other scenarios.

**The main goals of zcompile are:**

- Absolute minimum sized output
- Tightly-packed self-containing code
- Achieve modularisation with native syntax through preprocessing
- Decompose generically using files and plain text
- Simple, strict, and speedy processes and opinions over interoperability and standards
- As native and lightweight as possible

**You will need to get comfortable at writing code that:**

- may put slightly more weight on optimisation than ease-of-use
- is a bit more basic and primitive
- probably won't play as nicely with IDEs and linters (for now)
- is not what you or anyone else typically writes
- may break some standards

**The result will be code that:**

- is nicer in a different way
- you have significantly more raw, predictable control over
- will be very compact and efficient if done correctly
- can be large and complex despite using basic, primitive processes
- allows you to think and design differently
