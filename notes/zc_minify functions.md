# `zc_minify_*` functions

- All functions must take the following arguments in order:
  1. `code`
  1. `settings`
  1. `ref`
- It assumes that:
  - Any `onload` for its type has already been called, and `code`
    is the final form of code that needs to be processed
- Its responsibilities are:
  - To return a Promise even if it isn't asynchronous
  - To minify only if `minify` is truthy (other responsibilities still
    need to be met)
  - To call `onload` and the relevant minifier for any nested code,
    even if it itself doesn't need to minify
    - Update the `pos` of any errors if necessary
  - To reject with an error object with an accurate `pos` field 
    relative to `code` if possible
