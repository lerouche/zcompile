# Parallelisation

Process:

1. Process directives
1. Transpile
1. Minify

Each step is atomic and can be delegated to a thread. Nothing during the process of a single step can be delegated to another thread.

A possible counterexample might be importing, since it does step 1 on a subfile. However, there is no point to this. Directives must be processed serially, so if a thread delegates importing to another thread, it still has to wait for that thread to finish before doing anything else, so it's simply idling and wasting time during the process.

It can't delegate multiple imports in parallel because they have to be processed serially; for example, a second import might depend on a variable set by the first import.

Also, importing requires state information such as variables and the stack, which is difficult to do cleanly and efficiently using IPC.

Importing can't launch their own subprocess (i.e. process, transpile, and minify before returning result to importer), because the source being imported might not be whole or valid syntax, or might not be used in such a way. For example, a special CSS directive might import a CSS file with only one block, and only use the properties in the block, not the block itself.
